/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    isNumberFinite,
} from "tripetto-runner-foundation";
import "./condition";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { IOperation, calculator } from "tripetto-block-calculator/runner";
import "./namespace/unmount";

export abstract class Number extends NodeBlock<{
    readonly prefill?: number;
    readonly operations?: IOperation[];
}> {
    /** Contains the calculator slot. */
    readonly calculatorSlot = this.valueOf<number, Slots.Numeric>(
        "calculator",
        "feature"
    );

    /** Contains the number slot with the value. */
    readonly numberSlot = assert(
        this.valueOf<number, Slots.Numeric>("number", "static", {
            prefill:
                (isNumberFinite(this.props.prefill) && {
                    value: this.props.prefill,
                }) ||
                undefined,
            onChange: (input) => {
                if (
                    this.calculatorSlot &&
                    input.slot instanceof Slots.Numeric
                ) {
                    calculator(
                        this.context,
                        this.props.operations || [],
                        input,
                        this.calculatorSlot,
                        (id) => this.variableFor(id),
                        (id) => this.parseVariables(id)
                    );
                }
            },
        })
    );

    /** Contains if the block is required. */
    readonly required = this.numberSlot.slot.required || false;
}
