/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/condition.svg";
import ICON_NUMBER from "../../assets/icon.svg";
import ICON_CALCULATOR from "../../assets/calculator.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:number", "Verify number");
    },
})
export class NumberCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    get icon() {
        return this.slot?.reference === "calculator"
            ? ICON_CALCULATOR
            : ICON_NUMBER;
    }

    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Numeric) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:number",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "defined":
                    return `@${slot.id} ${pgettext(
                        "block:number",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${slot.id} ${pgettext("block:number", "empty")}`;
                case "not-equal":
                    return `@${slot.id} \u2260 ${value}`;
                case "above":
                case "below":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    private parse(
        slot: Slots.Numeric,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.toString(value, (n, p) =>
                L10n.locale.number(n, p, false)
            );
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return "@" + value;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        const isCalculator = this.slot?.reference === "calculator";

        this.editor.form({
            title: pgettext("block:number", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is equal to"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is equal to"
                                  ),
                            value: "equal",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is not equal to"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is not equal to"
                                  ),
                            value: "not-equal",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is lower than"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is lower than"
                                  ),
                            value: "below",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is higher than"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is higher than"
                                  ),
                            value: "above",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is between"
                                  )
                                : pgettext("block:number", "Number is between"),
                            value: "between",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is not between"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is not between"
                                  ),
                            value: "not-between",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is valid"
                                  )
                                : pgettext(
                                      "block:number",
                                      "Number is not empty"
                                  ),
                            value: "defined",
                        },
                        {
                            label: isCalculator
                                ? pgettext(
                                      "block:number",
                                      "Calculation is not valid"
                                  )
                                : pgettext("block:number", "Number is empty"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    from.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    to.visible(
                        mode.value === "between" || mode.value === "not-between"
                    );

                    switch (mode.value) {
                        case "equal":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation equals"
                                  )
                                : pgettext("block:number", "If number equals");
                            break;
                        case "not-equal":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation not equals"
                                  )
                                : pgettext(
                                      "block:number",
                                      "If number not equals"
                                  );
                            break;
                        case "below":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation is lower than"
                                  )
                                : pgettext(
                                      "block:number",
                                      "If number is lower than"
                                  );
                            break;
                        case "above":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation is higher than"
                                  )
                                : pgettext(
                                      "block:number",
                                      "If number is higher than"
                                  );
                            break;
                        case "between":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation is between"
                                  )
                                : pgettext(
                                      "block:number",
                                      "If number is between"
                                  );
                            break;
                        case "not-between":
                            from.title = isCalculator
                                ? pgettext(
                                      "block:number",
                                      "If calculation is not between"
                                  )
                                : pgettext(
                                      "block:number",
                                      "If number is not between"
                                  );
                            break;
                    }
                }),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const src = this.slot as Slots.Numeric | undefined;
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:number", "Use fixed number"))
                .precision(src?.precision || 0)
                .digits(src?.digits || 0)
                .decimalSign(src?.decimal || "")
                .thousands(src?.separator ? true : false, src?.separator || "")
                .prefix(src?.prefix || "")
                .prefixPlural(src?.prefixPlural || undefined)
                .suffix(src?.suffix || "")
                .suffixPlural(src?.suffixPlural || undefined)
                .min(src?.minimum)
                .max(src?.maximum)
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isString(value) ? value : undefined,
                false,
                this.slot?.id
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:number", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"number" | "variable">(
                            [
                                {
                                    label: pgettext("block:number", "Number"),
                                    value: "number",
                                },
                                {
                                    label: pgettext("block:number", "Value"),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value) ? "variable" : "number"
                        ).on((type) => {
                            numberControl.visible(type.value === "number");
                            variableControl.visible(type.value === "variable");

                            if (numberControl.isObservable) {
                                numberControl.focus();
                            }
                        }),
                        numberControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            isCalculator
                ? pgettext("block:number", "If calculation equals")
                : pgettext("block:number", "If number equals"),
            this.mode !== "defined" && this.mode !== "undefined"
        );
        const to = addCondition(
            "to",
            pgettext("block:number", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
