/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    Slots,
    conditions,
    definition,
    each,
    editor,
    isNumber,
    isNumberFinite,
    isString,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { NumberCondition } from "./condition";
import { TMode } from "../runner/mode";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { ICalculator, Operation, calculator } from "tripetto-block-calculator";
import "./namespace/unmount";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_CALCULATOR from "../../assets/calculator.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:number", "Number");
    },
})
export class Number extends NodeBlock implements ICalculator {
    readonly startBlank = false;
    numberSlot!: Slots.Numeric;

    @definition("number", "optional")
    prefill?: number;

    @definition
    readonly operations: Collection.Provider<Operation, ICalculator> =
        Collection.of<Operation, ICalculator>(Operation, this);

    get block() {
        return this;
    }

    get firstANS() {
        return "@" + this.numberSlot.id;
    }

    @slots
    defineSlot(): void {
        this.numberSlot = this.slots.static({
            type: Slots.Numeric,
            reference: "number",
            label: Number.label,
            exchange: [
                "required",
                "alias",
                "exportable",
                "precision",
                "digits",
                "decimal",
                "separator",
                "minimum",
                "maximum",
                "prefix",
                "prefixPlural",
                "suffix",
                "suffixPlural",
            ],
        });
    }

    @editor
    defineEditor(): void {
        const minimum = new Forms.Numeric(
            Forms.Numeric.bind(this.numberSlot, "minimum", undefined)
        )
            .precision(this.numberSlot.precision || 0)
            .digits(this.numberSlot.digits || 0)
            .decimalSign(this.numberSlot.decimal || "")
            .thousands(
                this.numberSlot.separator ? true : false,
                this.numberSlot.separator || ""
            )
            .prefix(this.numberSlot.prefix || "")
            .prefixPlural(this.numberSlot.prefixPlural || undefined)
            .suffix(this.numberSlot.suffix || "")
            .suffixPlural(this.numberSlot.suffixPlural || undefined)
            .label(pgettext("block:number", "Minimum"));

        const maximum = new Forms.Numeric(
            Forms.Numeric.bind(this.numberSlot, "maximum", undefined)
        )
            .precision(this.numberSlot.precision || 0)
            .digits(this.numberSlot.digits || 0)
            .decimalSign(this.numberSlot.decimal || "")
            .thousands(
                this.numberSlot.separator ? true : false,
                this.numberSlot.separator || ""
            )
            .prefix(this.numberSlot.prefix || "")
            .prefixPlural(this.numberSlot.prefixPlural || undefined)
            .suffix(this.numberSlot.suffix || "")
            .suffixPlural(this.numberSlot.suffixPlural || undefined)
            .label(pgettext("block:number", "Maximum"));

        const prefillValue = new Forms.Numeric(
            Forms.Numeric.bind(this, "prefill", undefined)
        )
            .precision(this.numberSlot.precision || 0)
            .digits(this.numberSlot.digits || 0)
            .decimalSign(this.numberSlot.decimal || "")
            .thousands(
                this.numberSlot.separator ? true : false,
                this.numberSlot.separator || ""
            )
            .prefix(this.numberSlot.prefix || "")
            .prefixPlural(this.numberSlot.prefixPlural || undefined)
            .suffix(this.numberSlot.suffix || "")
            .suffixPlural(this.numberSlot.suffixPlural || undefined)
            .min(this.numberSlot.minimum)
            .max(this.numberSlot.maximum);

        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:number", "Format"),
            form: {
                title: pgettext("block:number", "Format"),
                controls: [
                    new Forms.Dropdown(
                        [
                            { optGroup: pgettext("block:number", "Decimals") },
                            { label: "#", value: 0 },
                            { label: "#.#", value: 1 },
                            { label: "#.##", value: 2 },
                            { label: "#.###", value: 3 },
                            { label: "#.####", value: 4 },
                            { label: "#.#####", value: 5 },
                            { label: "#.######", value: 6 },
                            { label: "#.#######", value: 7 },
                            { label: "#.########", value: 8 },
                            { optGroup: pgettext("block:number", "Digits") },
                            { label: "##", value: -2 },
                            { label: "###", value: -3 },
                            { label: "####", value: -4 },
                            { label: "#####", value: -5 },
                            { label: "######", value: -6 },
                            { label: "#######", value: -7 },
                            { label: "########", value: -8 },
                            { label: "#########", value: -9 },
                            { label: "##########", value: -10 },
                            { label: "###########", value: -11 },
                            { label: "############", value: -12 },
                            { label: "#############", value: -13 },
                            { label: "##############", value: -14 },
                            { label: "###############", value: -15 },
                            { label: "################", value: -16 },
                        ],
                        (this.numberSlot.digits
                            ? -this.numberSlot.digits
                            : this.numberSlot.precision) || 0
                    ).on((format: Forms.Dropdown<number>) => {
                        this.numberSlot.precision =
                            format.isFeatureEnabled &&
                            isNumber(format.value) &&
                            format.value >= 0
                                ? format.value
                                : undefined;
                        this.numberSlot.digits =
                            format.isFeatureEnabled &&
                            isNumber(format.value) &&
                            format.value < 0
                                ? -format.value
                                : undefined;

                        minimum.precision(this.numberSlot.precision || 0);
                        maximum.precision(this.numberSlot.precision || 0);
                        prefillValue.precision(this.numberSlot.precision || 0);
                        minimum.digits(this.numberSlot.digits || 0);
                        maximum.digits(this.numberSlot.digits || 0);
                        prefillValue.digits(this.numberSlot.digits || 0);
                        decimal.disabled(!this.numberSlot.precision);
                        signs.disabled((this.numberSlot.digits || 0) > 0);
                    }),
                ],
            },
            activated:
                isNumber(this.numberSlot.precision) ||
                isNumber(this.numberSlot.digits),
        });

        const decimal = new Forms.Dropdown(
            [
                { label: "#.#", value: "." },
                { label: "#,#", value: "," },
            ],
            Forms.Dropdown.bind(this.numberSlot, "decimal", undefined)
        )
            .label(pgettext("block:number", "Decimal sign"))
            .disabled(!this.numberSlot.precision)
            .on(() => {
                minimum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
                maximum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
                prefillValue.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
            });

        this.editor.option({
            name: pgettext("block:number", "Limits"),
            form: {
                title: pgettext("block:number", "Limits"),
                controls: [minimum, maximum],
            },
            activated:
                isNumber(this.numberSlot.minimum) ||
                isNumber(this.numberSlot.maximum),
        });

        const prefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "prefix", undefined)
        )
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefix((p.isFeatureEnabled && p.value) || "");
                maximum.prefix((p.isFeatureEnabled && p.value) || "");
                prefillValue.prefix((p.isFeatureEnabled && p.value) || "");
            });
        const pluralPrefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "prefixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
                maximum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
                prefillValue.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:number", "Prefix when value is plural")
            )
            .visible(isString(this.numberSlot.prefixPlural));

        this.editor.option({
            name: pgettext("block:number", "Prefix"),
            form: {
                title: pgettext("block:number", "Prefix"),
                controls: [
                    prefix,
                    new Forms.Checkbox(
                        pgettext(
                            "block:number",
                            "Specify different prefix for plural values"
                        ),
                        isString(this.numberSlot.prefixPlural)
                    ).on((c) => {
                        prefix.placeholder(
                            (c.isChecked &&
                                pgettext(
                                    "block:number",
                                    "Prefix when value is singular"
                                )) ||
                                ""
                        );

                        pluralPrefix.visible(c.isChecked);
                    }),
                    pluralPrefix,
                ],
            },
            activated: isString(this.numberSlot.prefix),
        });

        const suffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "suffix", undefined)
        )
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffix((s.isFeatureEnabled && s.value) || "");
                maximum.suffix((s.isFeatureEnabled && s.value) || "");
                prefillValue.suffix((s.isFeatureEnabled && s.value) || "");
            });
        const pluralSuffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "suffixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
                maximum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
                prefillValue.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:number", "Suffix when value is plural")
            )
            .visible(isString(this.numberSlot.suffixPlural));

        this.editor.option({
            name: pgettext("block:number", "Suffix"),
            form: {
                title: pgettext("block:number", "Suffix"),
                controls: [
                    suffix,
                    new Forms.Checkbox(
                        pgettext(
                            "block:number",
                            "Specify different suffix for plural values"
                        ),
                        isString(this.numberSlot.suffixPlural)
                    ).on((c) => {
                        suffix.placeholder(
                            (c.isChecked &&
                                pgettext(
                                    "block:number",
                                    "Suffix when value is singular"
                                )) ||
                                ""
                        );

                        pluralSuffix.visible(c.isChecked);
                    }),
                    pluralSuffix,
                ],
            },
            activated: isString(this.numberSlot.suffix),
        });

        const signs = this.editor.option({
            name: pgettext("block:number", "Signs"),
            form: {
                title: pgettext("block:number", "Signs"),
                controls: [
                    decimal,
                    new Forms.Dropdown(
                        [
                            {
                                label: pgettext("block:number", "None"),
                                value: undefined,
                            },
                            { label: "#,###", value: "," },
                            { label: "#.###", value: "." },
                        ],
                        Forms.Dropdown.bind(
                            this.numberSlot,
                            "separator",
                            undefined
                        )
                    )
                        .label(pgettext("block:number", "Thousands separator"))
                        .on((separator) => {
                            minimum.thousands(
                                separator.isFeatureEnabled && separator.value
                                    ? true
                                    : false,
                                (separator.isFeatureEnabled &&
                                    separator.value) ||
                                    ""
                            );
                            maximum.thousands(
                                separator.isFeatureEnabled && separator.value
                                    ? true
                                    : false,
                                (separator.isFeatureEnabled &&
                                    separator.value) ||
                                    ""
                            );
                            prefillValue.thousands(
                                separator.isFeatureEnabled && separator.value
                                    ? true
                                    : false,
                                (separator.isFeatureEnabled &&
                                    separator.value) ||
                                    ""
                            );
                        }),
                    new Forms.Static(
                        pgettext(
                            "block:number",
                            "**Note:** These signs are used to format the number in de dataset. When the number is displayed in a runner, the appropriate user locale might be applied making it seem like changing these settings has no effect."
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.numberSlot.separator) ||
                isString(this.numberSlot.decimal),
            disabled: (this.numberSlot.digits || 0) > 0,
        });

        this.editor.groups.options();

        this.editor.option({
            name: pgettext("block:number", "Prefill"),
            form: {
                title: pgettext("block:number", "Prefill"),
                controls: [
                    prefillValue,
                    new Forms.Static(
                        pgettext(
                            "block:number",
                            "This value will be used as initial (or default) value for the number."
                        )
                    ),
                ],
            },
            activated: isNumberFinite(this.prefill),
        });

        this.editor.numeric({
            target: this,
            reference: "calculator",
            label: pgettext("block:number", "Calculation"),
            name: pgettext("block:number", "Calculator"),
            title: pgettext("block:number", "Calculator settings"),
            exportable: true,
            pair: this.editor.collection(
                calculator(
                    this,
                    pgettext(
                        "block:number",
                        "Perform a calculation with the number value."
                    ),
                    pgettext("block:number", "Calculator")
                )
            ),
        });

        this.editor.required(this.numberSlot);
        this.editor.visibility();
        this.editor.alias(this.numberSlot);
        this.editor.exportable(this.numberSlot);
    }

    @conditions
    defineCondition(): void {
        const calculatorSlot = this.slots.select("calculator", "feature");
        const group = calculatorSlot
            ? this.conditions.group(this.numberSlot.label || Number.label)
            : this.conditions;

        each(
            [
                {
                    mode: "equal",
                    label: pgettext("block:number", "Number is equal to"),
                },
                {
                    mode: "not-equal",
                    label: pgettext("block:number", "Number is not equal to"),
                },
                {
                    mode: "below",
                    label: pgettext("block:number", "Number is lower than"),
                },
                {
                    mode: "above",
                    label: pgettext("block:number", "Number is higher than"),
                },
                {
                    mode: "between",
                    label: pgettext("block:number", "Number is between"),
                },
                {
                    mode: "not-between",
                    label: pgettext("block:number", "Number is not between"),
                },
                {
                    mode: "defined",
                    label: pgettext("block:number", "Number is not empty"),
                },
                {
                    mode: "undefined",
                    label: pgettext("block:number", "Number is empty"),
                },
            ],
            (condition: { mode: TMode; label: string }) => {
                group.template({
                    condition: NumberCondition,
                    icon: ICON,
                    label: condition.label,
                    autoOpen:
                        condition.mode !== "defined" &&
                        condition.mode !== "undefined",
                    props: {
                        slot: this.numberSlot,
                        mode: condition.mode,
                    },
                });
            }
        );

        if (calculatorSlot) {
            const calculatorGroup = this.conditions.group(
                calculatorSlot.label || pgettext("block:number", "Calculation")
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:number",
                            "Calculation is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:number",
                            "Calculation is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:number",
                            "Calculation is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:number",
                            "Calculation is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:number",
                            "Calculation is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:number",
                            "Calculation is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext("block:number", "Calculation is valid"),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:number",
                            "Calculation is not valid"
                        ),
                    },
                ],
                (condition: { mode: TMode; label: string }) => {
                    calculatorGroup.template({
                        condition: NumberCondition,
                        icon: ICON_CALCULATOR,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: calculatorSlot,
                            mode: condition.mode,
                        },
                    });
                }
            );
        }
    }
}
